#!/bin/sh
# Released under the NYSL Ver 0.9982

# root pssword for MySQL
db_pass='password'
# zabbix password for MySQL
zabbix_pass='password'

# Install MySQL
DEBIAN_FRONTEND=noninteractive apt install mysql-server -y

# MySQL Setup
mysql -uroot -e "UPDATE mysql.user SET Password=PASSWORD('${db_pass}') WHERE User='root';"
mysql -uroot -e "FLUSH PRIVILEGES;"
mysql -uroot -p${db_pass} -e "DELETE FROM mysql.user WHERE User='';"
mysql -uroot -p${db_pass} -e "DELETE FROM mysql.user WHERE User='root' AND Host NOT IN ('localhost', '127.0.0.1', '::1');"
#mysql -uroot -p${db_pass} -e "DROP DATABASE test;"
mysql -uroot -p${db_pass} -e "DELETE FROM mysql.db WHERE Db='test' OR Db='test\\_%'"
mysql -uroot -p${db_pass} -e "FLUSH PRIVILEGES;"

# Install Zabbix Server
wget https://repo.zabbix.com/zabbix/3.4/debian/pool/main/z/zabbix-release/zabbix-release_3.4-1+stretch_all.deb
dpkg -i zabbix-release_3.4-1+stretch_all.deb
apt update

apt install zabbix-server-mysql -y

# Create CB and DB user
mysql -u root -p${db_pass} -e "CREATE DATABASE zabbix CHARACTER SET utf8;"
mysql -u root -p${db_pass} -e "GRANT ALL PRIVILEGES ON zabbix.* TO zabbix@localhost identified by '${zabbix_pass}';"
mysql -u root -p${db_pass} -e "FLUSH PRIVILEGES;"

# Create DB table
zcat /usr/share/doc/zabbix-server-mysql/create.sql.gz | mysql -uroot -p${db_pass} zabbix

# Set password for DB user
sed -ie "s/# DBPassword=/DBPassword=${zabbix_pass}/" /etc/zabbix/zabbix_server.conf

# Start zabbix-server
service zabbix-server start

# Install zabbix-frontend, zabbix-agent
apt install zabbix-frontend-php zabbix-agent ufw -y

sed -ie 's|# php_value date.timezone Europe/Riga|php_value date.timezone Asia/Tokyo|' /etc/apache2/conf-enabled/zabbix.conf

service apache2 restart

# Set firewall
ufw default DENY
ufw allow ssh
ufw allow www
ufw allow 10050/tcp
ufw allow 10051/tcp

echo y | ufw enable 1
